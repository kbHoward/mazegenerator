# MazeGenerator
![Alt text](ss2.png?raw=true)

## Description
A randomly generated maze using Depth First Search and a recursive algorithm. Rendered with SFML.

## Installation Dependencies
### Ubuntu / PopOS 22.04 LTS
`apt-get install g++ cmake make git libsfml-dev`

## Cloning the Repository & Compiling
`git clone https://gitlab.com/kbhoward/mazegenerator.git`

`cd mazegenerator`

`mkdir build && cd build`

`cmake ..`

`make`

## Execution (Linux)
`./mazegenerator`

## Support
* Press [Spacebar] to generate a new maze.
* Press [Q] to quit the program.

## Contributing

## License
MIT License
