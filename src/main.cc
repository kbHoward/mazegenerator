#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/VideoMode.hpp>

#include <algorithm>
#include <cstdlib>
#include <random>
#include <array>

struct cell {
    bool visited = false;
    bool entry = false;
    bool exit = false;
    // top right bottom left
    bool walls[4] = {true,true,true,true}; 
} cell;

const int WIDTH = 1080, HEIGHT = WIDTH;
int cell_size = WIDTH * 0.04; //ratio
bool running = false;

class Maze {
    public:
        Maze(unsigned int, unsigned int);
        bool bound(const unsigned int, const unsigned int);
        void Carve(int, int);
        void MakeGoals();
        std::array<int,2> Gbeg(){return beg;}
        std::array<int,2> Gend(){return end;}
        void ClearGrid(){this->grid.clear();}
        void ResetMaze();
        
        std::vector<std::vector<struct cell>> Grid(){return grid;}
        unsigned int rows;
        unsigned int cols;

    private:
        std::vector<std::vector<struct cell>> grid;
        std::array<int,2>beg;
        std::array<int,2>end;
};

Maze::Maze(unsigned int r, unsigned int c) : rows(r), cols(c){
    grid.reserve(r*c);
    for (unsigned i = 0; i < rows; i++){
        std::vector<struct cell> m;
        for (unsigned k = 0; k < cols; k++){
            struct cell tmp;
            m.push_back(tmp);
        }
        grid.push_back(m);
    }
}

bool Maze::bound(const unsigned int x, const unsigned int y){
    if (x > this->rows -1) return false;
    if (y > this->cols-1) return false;

    return true;
}

void Maze::Carve(int x, int y) {
    grid[x][y].visited = true;
    int nx, ny;
    int opp = -1;

    std::array<int, 4> dir {0,1,2,3};
    std::random_shuffle(std::begin(dir), std::end(dir));

    for (int i = 0; i < 4; i++){
        switch(dir[i]){
            case 0:
                nx = x;
                ny = y -1;
                opp = 2;
                break;
            case 1:
                nx = x+1;
                ny = y;
                opp = 3;
                break;
            case 2:
                nx = x;
                ny = y+1;
                opp = 0;
                break;
            case 3:
                nx = x-1;
                ny = y;
                opp = 1;
                break;
        };
        if (bound(nx, ny) && grid[nx][ny].visited == false) {
            grid[nx][ny].walls[opp] = false;
            grid[x][y].walls[dir[i]] = false;
            Carve(nx, ny);
        }
    }
}

void Maze::ResetMaze(){
    for (size_t y = 0; y < grid.size(); y++){
        for (size_t x = 0; x < grid[y].size(); x++){
            struct cell tmp;
            grid[y][x] = tmp;
        }
    }
}

void Maze::MakeGoals() {
    std::array<int, 2> dir {0,1};
    std::random_shuffle(std::begin(dir), std::end(dir));

    switch(dir[0]){
        case 0:
            grid[0][0].walls[3] = false;
            grid[HEIGHT/cell_size-1][WIDTH/cell_size-1].walls[2] = false;
            break;
        case 1:
            grid[0][WIDTH/cell_size-1].walls[2] = false;
            grid[HEIGHT/cell_size-1][0].walls[1] = false;
            break;

        default:
            break;
    };
}

class Line : public sf::Vertex {
    public:
        Line(){};
        Line(sf::Vector2f, sf::Vector2f);
        sf::Vertex vert[2];
};

Line::Line(sf::Vector2f v, sf::Vector2f v2){
    vert[0] = v;
    vert[1] = v2;
}

void render(sf::RenderWindow &window, Maze *m) {
    std::vector<std::vector<struct cell>> g = m->Grid();

    window.clear(sf::Color(0,0,0));

        for (unsigned int i = 0; i < m->rows; i++){
            for (unsigned int j = 0; j < m->cols; j++){
                std::array<Line, 4> cell_walls;
                for (auto w : cell_walls){
                    int x = i * cell_size;
                    int y = j * cell_size;

                    w.vert[0] = sf::Vector2f(0,0);
                    w.vert[1] = sf::Vector2f(0,0);


                    if (g[i][j].entry) {
                        sf::RectangleShape r(sf::Vector2f(cell_size-1,cell_size-1));
                        r.setFillColor(sf::Color::Blue);
                        r.setPosition(x,y);
                        window.draw(r);
                    }
                    if (g[i][j].exit) {
                        sf::RectangleShape r(sf::Vector2f(cell_size-1,cell_size-1));
                        r.setFillColor(sf::Color::Red);
                        r.setPosition(x,y);
                        window.draw(r);
                    }
                    //top
                    if (g[i][j].walls[0]){
                        w.vert[0] = sf::Vector2f(x,y);
                        w.vert[1] = sf::Vector2f(x+cell_size,y);
                        window.draw(w.vert, 2,sf::Lines);
                    }
                    //right
                    if (g[i][j].walls[1]){
                        w.vert[0] = sf::Vector2f(x+cell_size,y);
                        w.vert[1] = sf::Vector2f(x+cell_size, y+cell_size);
                        window.draw(w.vert, 2,sf::Lines);
                    }
                    //bottom
                    if (g[i][j].walls[2]) {
                        w.vert[0] = sf::Vector2f(x+cell_size,y+cell_size);
                        w.vert[1] = sf::Vector2f(x, y+cell_size);
                        window.draw(w.vert, 2,sf::Lines);
                    }
                    //left
                    if (g[i][j].walls[3]) {
                        w.vert[0] = sf::Vector2f(x,y+cell_size);
                        w.vert[1] = sf::Vector2f(x, y);
                        window.draw(w.vert, 2,sf::Lines);
                    }
                }
            }
        }
    window.display();
}

void KeyInput(sf::Keyboard::Key &key, Maze *m){
    switch (key) {
        case sf::Keyboard::Space:
            m->ResetMaze();
            m->Carve(0,0);
            m->MakeGoals();
            break;

        case sf::Keyboard::Q:
            running = false;
            break;

        default:
            break;
    }
}

void pollEvents(sf::RenderWindow &win, Maze *m){

    sf::Event event;
    while (win.pollEvent(event))
    {
        switch (event.type) {
            case sf::Event::Closed:
                win.close();
                running = false;
                break;

            case sf::Event::KeyReleased:
                KeyInput(event.key.code, m);
                break;
            default:
                break;
        }
    }
}


void run(sf::RenderWindow *win, Maze *m){
    running = true;

    while (running && win->isOpen()) {
        pollEvents(*win, m);
        render(*win, m);
    }
}

int main() {
    srand(time(NULL));

    Maze m(WIDTH/cell_size, HEIGHT/cell_size);

    m.MakeGoals();

    sf::RenderWindow window(sf::VideoMode(800, 600),"MazeGenerator");
    window.setVerticalSyncEnabled(1);

    sf::View v1(sf::FloatRect(0,0,WIDTH,HEIGHT));
    v1.setViewport(sf::FloatRect(0.05f,0.05f,0.9f,0.9f));
    v1.zoom(-1.f);
    
    window.setView(v1);
    m.Carve(0,0);
    run(&window, &m);
}
